namespace ShapeSolid.Model
{
    public abstract class AbstractShape : ICalculate
    {
        public abstract ExtensionShapeInfo GetShapeInfo();
        public abstract double Area();
        public abstract double Perimeter();
    }
}