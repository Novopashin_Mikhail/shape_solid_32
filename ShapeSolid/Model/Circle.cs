using System;

namespace ShapeSolid.Model
{
    public class Circle : AbstractShape
    {
        public int R { get; set; }
        
        public override double Area()
        {
            return Math.PI * R;
        }

        public override double Perimeter()
        {
            return 2 * Math.PI * R;
        }

        public override ExtensionShapeInfo GetShapeInfo()
        {
            return new ExtensionShapeInfo
            {
                Title = "Круг",
                AbbreviationForCreate = "crc",
                MessageForParams = "Введите радиус круга (целое число)",
                CountParamsForCalc = 1
            };
        }
    }
}