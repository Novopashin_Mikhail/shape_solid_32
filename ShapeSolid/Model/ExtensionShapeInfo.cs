namespace ShapeSolid.Model
{
    public class ExtensionShapeInfo
    {
        public string MessageForParams { get; set; }
        public string AbbreviationForCreate { get; set; }
        public string Title { get; set; }
        public int CountParamsForCalc { get; set; }
    }
}