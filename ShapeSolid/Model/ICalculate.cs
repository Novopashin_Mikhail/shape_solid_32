namespace ShapeSolid.Model
{
    public interface ICalculate
    {
        public double Area();
        public double Perimeter();
    }
}