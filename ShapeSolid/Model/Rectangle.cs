namespace ShapeSolid.Model
{
    public class Rectangle : AbstractShape
    {
        public int A { get; set; }
        public int B { get; set; }

        public override double Area()
        {
            return A * B;
        }

        public override double Perimeter()
        {
            return 2 * (A + B);
        }
        
        public override ExtensionShapeInfo GetShapeInfo()
        {
            return new ExtensionShapeInfo
            {
                Title = "Прямоугольник",
                AbbreviationForCreate = "rc",
                MessageForParams = "Введите 2 стороны прямоугольника через пробел (целые числа)",
                CountParamsForCalc = 2
            };
        }
    }
}