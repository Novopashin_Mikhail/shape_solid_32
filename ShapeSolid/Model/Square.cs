namespace ShapeSolid.Model
{
    public class Square : AbstractShape
    {
        public int Side { get; set; }
        public override double Area()
        {
            return Side * Side;
        }

        public override double Perimeter()
        {
            return 4 * Side;
        }
        
        public override ExtensionShapeInfo GetShapeInfo()
        {
            return new ExtensionShapeInfo
            {
                Title = "Квадрат",
                AbbreviationForCreate = "sq",
                MessageForParams = "Введите сторону квадрата (целое число)",
                CountParamsForCalc = 1
            };
        }
    }
}