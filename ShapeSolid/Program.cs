﻿using System;
using ShapeSolid.Service;

namespace ShapeSolid
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new CommunicationService(new Shower(new ConsolePrinter()),new ConsoleReader(),new SimpleCreator(),new Validator());
            service.Run();
        }
    }
}