using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public class CommunicationService : ICommunication
    {
        private IShower _shower; 
        private IReader _reader;
        private ICreator _creator;
        private IValidator _validator;
        
        public CommunicationService(IShower shower, IReader reader, ICreator creator, IValidator validator)
        {
            _shower = shower;       
            _reader = reader;       
            _creator = creator;     
            _validator = validator; 
        }
        
        public void Run()
        {
            _shower.Greeting();
            while (true)
            {
                var inputShape = SelectShape();
                if (IsBreak(inputShape)) break;
                var objShape = _creator.Create(inputShape);
                var inputParams = string.Empty;
                InputParams(objShape, out inputParams);
                if (IsBreak(inputParams)) break;
                ShowResult(objShape);
            }
        }
        
        string SelectShape()
        {
            _shower.InputShape();
            var input = _reader.Read();
            while (!_validator.IsValidShape(input))
            {
                if (IsBreak(input)) break;
                _shower.InputShape();
                input = _reader.Read();
            }

            return input;
        }

        void InputParams(AbstractShape objAbstractShape, out string inputParams)
        {
            _shower.InputParams(objAbstractShape);
            inputParams = _reader.Read();
                
            while (!_validator.IsValidParams(objAbstractShape, inputParams))
            {
                if (IsBreak(inputParams)) break;
                inputParams = _reader.Read();
            }
            if (!IsBreak(inputParams))
                _creator.SetShapeParams(inputParams);
        }

        void ShowResult(AbstractShape objAbstractShape)
        {
            _shower.Show(objAbstractShape);
        }
                
        bool IsBreak(string input)
        {
            return input == "exit";
        }
    }
}