using System;

namespace ShapeSolid.Service
{
    public class ConsolePrinter : IPrinter
    {
        public void Print(string str)
        {
            Console.Write(str);
        }

        public void NextLine()
        {
            Console.WriteLine();
        }
    }
}