using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public abstract class Creator : ICreator
    {
        public abstract AbstractShape Create(string param);

        public abstract void SetShapeParams(string param);
    }
}