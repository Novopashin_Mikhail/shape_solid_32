using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public class SimpleCreator : Creator
    {
        private AbstractShape _abstractShape;
        
        public override AbstractShape Create(string param)
        {
            switch (param)
            {
                case "sq":
                    _abstractShape = new Square();
                    break;
                case "rc":
                    _abstractShape = new Rectangle();
                    break;
                case "crc":
                    _abstractShape = new Circle();
                    break;
            }

            return _abstractShape;
        }

        public override void SetShapeParams(string param)
        {
            switch (_abstractShape)
            {
                case Circle circle:
                    circle.R = int.Parse(param);
                    break;
                case Square square:
                    square.Side = int.Parse(param);
                    break;
                case Rectangle rectangle:
                    var arr = param.Split(" ");
                    rectangle.A = int.Parse(arr[0]);
                    rectangle.B = int.Parse(arr[1]);
                    break;
            }
        }
    }
}