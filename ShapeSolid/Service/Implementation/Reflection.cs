using System;
using System.Collections.Generic;
using System.Linq;
using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public static class Reflection
    {
        private static List<AbstractShape> _list = new List<AbstractShape>();
        
        public static IList<AbstractShape> GetShapes()
        {
            if (_list.Any())
                return _list;
            
            var type = typeof(AbstractShape);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));

            foreach (var itemType in types)
            {
                if (itemType == typeof(AbstractShape)) continue;
                var iShape = Activator.CreateInstance(itemType) as AbstractShape;
                if (iShape == null) continue;
                _list.Add(iShape);
            }

            return _list;
        }
    }
}