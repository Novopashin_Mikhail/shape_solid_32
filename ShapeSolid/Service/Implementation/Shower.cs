using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public class Shower : IShower
    {
        private IPrinter _printer;
        
        public Shower(IPrinter printer)
        {
            _printer = printer;
        }

        public void Greeting()
        {
            _printer.Print($"Программа расчета площади и периметра фигур");
            _printer.NextLine();
        }

        public void InputShape()
        {
            _printer.Print($"Для расчета:");
            _printer.NextLine();
            ShapesCanCreate();
            _printer.Print("Для выхода наберите exit");
            _printer.NextLine();
            _printer.NextLine();
            _printer.NextLine();
        }

        private void ShapesCanCreate()
        {
            var shapes = Reflection.GetShapes();
            foreach (var itemShapes in shapes)
            {
                _printer.Print($"{itemShapes.GetShapeInfo().Title} {itemShapes.GetShapeInfo().AbbreviationForCreate}");
                _printer.NextLine();    
            }
        }

        public void InputParams(AbstractShape abstractShape)
        {
            _printer.Print(abstractShape.GetShapeInfo().MessageForParams);
            _printer.NextLine();
        }
        
        public void Show(AbstractShape abstractShape)
        {
            _printer.Print($"Площадь = {abstractShape.Area()}");
            _printer.NextLine();
            _printer.Print($"Периметр = {abstractShape.Perimeter()}");
            _printer.NextLine();
            _printer.NextLine();
            _printer.NextLine();
        }
    }
}