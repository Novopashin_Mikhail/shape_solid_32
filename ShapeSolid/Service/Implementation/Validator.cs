using System.Linq;
using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public class Validator : IValidator
    {
        public bool IsValidShape(string shape)
        {
            var shapes = Reflection.GetShapes();
            return shapes.Any(x => x.GetShapeInfo().AbbreviationForCreate == shape);
        }

        public bool IsValidParams(AbstractShape abstractShape, string @params)
        {
            if (@params == null && (@params == null || string.IsNullOrEmpty(@params))) return false;

            var paramArray = @params.Split(" ");
            foreach (var item in paramArray)
            {
                if (!int.TryParse(item, out var itemInt))
                {
                    return false;
                }
            }

            return IsValidParamsForSelectedShape(abstractShape, @params);
        }

        private bool IsValidParamsForSelectedShape(AbstractShape abstractShape, string @params)
        {
            return @params.Split(" ").Length == abstractShape.GetShapeInfo().CountParamsForCalc;
        }
    }
}