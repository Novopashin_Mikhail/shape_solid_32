using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public interface ICreator
    {
        AbstractShape Create(string param);
        void SetShapeParams(string param);
    }
}