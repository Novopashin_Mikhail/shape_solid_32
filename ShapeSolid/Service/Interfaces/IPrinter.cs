namespace ShapeSolid.Service
{
    public interface IPrinter
    {
        void Print(string str);
        void NextLine();
    }
}