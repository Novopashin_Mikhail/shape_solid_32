namespace ShapeSolid.Service
{
    public interface IReader
    {
        string Read();
    }
}