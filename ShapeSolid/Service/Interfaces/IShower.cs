using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public interface IShower
    {
        void Show(AbstractShape abstractShape);
        void Greeting();
        void InputShape();
        void InputParams(AbstractShape abstractShape);
    }
}