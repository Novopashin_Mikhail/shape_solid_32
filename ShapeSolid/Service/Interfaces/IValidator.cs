using ShapeSolid.Model;

namespace ShapeSolid.Service
{
    public interface IValidator
    {
        bool IsValidShape(string shape);
        bool IsValidParams(AbstractShape abstractShape, string @params);
    }
}